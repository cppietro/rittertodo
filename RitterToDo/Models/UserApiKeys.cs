﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RitterToDo.Models
{
    public class UserApiKeys
    {

        
       // [ForeignKey("Category")]
        public Guid UserId { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        [StringLength(100)]
        public string AppName { get; set; }
            
    }
}