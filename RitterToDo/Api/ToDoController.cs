﻿using Moo;
using RitterToDo.Core;
using RitterToDo.Models;
using RitterToDo.Repos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Security;


namespace RitterToDo.Api
{
    public class ToDoController : ApiController
    {
        public IRepository<ToDo> ToDoRepo { get; private set; }

        public IMappingRepository MappingRepository { get; private set; }

        public ILookupHelper<ToDoCategory, ToDoCategoryViewModel> CategoryHelper { get; private set; }

        public ToDoController(
            IRepository<ToDo> todoRepo,
            ILookupHelper<ToDoCategory, ToDoCategoryViewModel> categoryHelper,
            IMappingRepository mappingRepository)
        {
            ToDoRepo = todoRepo;
            CategoryHelper = categoryHelper;
            MappingRepository = mappingRepository;
        }

        // GET api/<controller>
        [HttpGet]
        //  [TokenValidationAttribute]
        public IEnumerable<ToDoViewModel> GetToDo()
        {
            ApplicationDbContext db = new ApplicationDbContext();

            var id  = db.Users.FirstOrDefault(x => x.UserName == User.Identity.Name).Id;
   
            var entities = ToDoRepo.GetAll().Where(x => x.OwnerId == id);

            var mapper = MappingRepository.ResolveMapper<ToDo, ToDoViewModel>();
            var models = mapper.MapMultiple(entities);
            return models.ToArray<ToDoViewModel>();
        }

        // GET api/<controller>/5
        //
        //[Route("api/todo/{id}")]

        // [TokenValidationAttribute]
        [HttpGet]
        public ToDoViewModel Get(string id)
        {
            var guid = Guid.Parse(id);
            var entitie = ToDoRepo.GetById(guid);
            var mapper = MappingRepository.ResolveMapper<ToDo, ToDoViewModel>();
            var model = mapper.Map(entitie);
            return model;
        }

        // POST api/<controller>
        [HttpPost]
        // [TokenValidationAttribute]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        [HttpPut]
        //[TokenValidationAttribute]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete]
        //[TokenValidationAttribute]
        public void Delete(int id)
        {
        }

    }
}